gecode-js
=========
[![License: MIT][mit-image]][mit-url] [![NPM version][npm-image]][npm-url]

This is the npm package for the [Emscripten](https://emscripten.org) version of [Gecode](https://www.gecode.org) wrapped using [emscripten_wrapper](https://github.com/sgratzl/emscripten_wrapper).

Usage
-----

Best used as part of [MiniZinc Packages](https://gitlab.com/minizinc/minizinc-webide).


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/gecode-js.git
cd gecode-js
npm install
```

**Build distribution packages**

```bash
npm run build
```

**Release distribution packages**

based on [release-it](https://github.com/release-it/release-it)

```bash
npm run release:patch
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[npm-image]: https://badge.fury.io/js/gecode-js.svg
[npm-url]: https://npmjs.org/package/gecode-js
