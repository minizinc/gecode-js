import {IFznGecodeModule} from "./internal/types";
export * from "./internal/types";

export const FZNGECODE: IFznGecodeModule;
export default FZNGECODE;
