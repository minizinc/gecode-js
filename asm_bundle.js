import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

export const FZNGECODE = initWrapper(createWrapper({
  module: () => import('./emscripten/fzn-gecode_asm.js'),
  mem: () => import('./emscripten/fzn-gecode_asm.js.mem')
}));

export default FZNGECODE;
