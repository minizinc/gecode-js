import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

export const FZNGECODE = initWrapper(createWrapper({
  module: () => import('./emscripten/fzn-gecode.js'),
  wasm: () => import('./emscripten/fzn-gecode.wasm')
}));

export default FZNGECODE;
