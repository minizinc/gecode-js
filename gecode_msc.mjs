const M = {
  "id": "org.gecode.gecode",
  "name": "Gecode",
  "description": "Gecode FlatZinc executable",
  "version": "6.2.0",
  "mznlib": "-Ggecode_presolver",
  "executable": "fzn-gecode",
  "tags": ["cp","int", "float", "set", "restart"],
  "stdFlags": ["-a","-f","-n","-p","-r","-s","-t"],
  "supportsMzn": false,
  "supportsFzn": true,
  "needsSolns2Out": true,
  "needsMznExecutable": false,
  "needsStdlibDir": false,
  "isGUIApplication": false
}

export default M;
