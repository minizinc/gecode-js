import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

const FZNGECODE = initWrapper(createWrapper(() => import('./emscripten/fzn-gecode')));

export default FZNGECODE;
