import {IAsyncEMWWrapper} from "emscripten_wrapper";
import {IExtendedWrapper} from "./types";


export function initWrapper<T extends IAsyncEMWWrapper>(wrapper: T): T & IExtendedWrapper;
