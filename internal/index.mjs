import config from '../gecode_msc';

export function initWrapper(wrapper) {
  wrapper.SOLVER_ID = 'gecode';
  wrapper.SOLVER_MSC_FILE = config;

  const superCreateWorkerClient = wrapper.createWorkerClient;
  wrapper.createWorkerClient = function () {
    var r = superCreateWorkerClient.apply(this, Array.from(arguments));

    r.SOLVER_ID = wrapper.SOLVER_ID;
    r.SOLVER_MSC_FILE = wrapper.SOLVER_MSC_FILE;
    return r;
  };

  return wrapper;
}
