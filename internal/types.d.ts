import {IAsyncEMWMainWrapper, ISyncEMWWrapper, IEMWMain, IEMWWorkerClient} from "emscripten_wrapper";
import {IEMWMainPromise} from "emscripten_wrapper";

export interface IExtendedWrapper {
  readonly SOLVER_ID: string;
  readonly SOLVER_MSC_FILE: any;
}

export declare type IFznGecodeWorkerClient = IEMWWorkerClient<{}> & IEMWMainPromise & IExtendedWrapper;
export declare type IFznGecodeModule = IAsyncEMWMainWrapper<{}> & IExtendedWrapper;
export declare type IFznGecodeSyncModule = ISyncEMWWrapper<{}> & IEMWMain;
